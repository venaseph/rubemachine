//
//  ViewController.swift
//  RubeMachine
//
//  Created by Chris Peragine on 2/3/18.
//  Copyright © 2018 Chris Peragine. All rights reserved.
//  View on iPhone 8 Em

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var arrow: UIImageView!
    @IBOutlet private weak var tyler: UIImageView!
    @IBOutlet private weak var flame: UIImageView!
    @IBOutlet private weak var anvil: UIImageView!
    @IBOutlet private weak var balloon: UIImageView!
    @IBOutlet private weak var box: UITextView!
    
    var start: CGPoint!
    var animate: UIViewPropertyAnimator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func handlePan(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: self.view)
        if let view = sender.view {
            view.center = CGPoint(x:view.center.x + translation.x, y:view.center.y + 0) // 0 ignores vertical
            if sender.state == UIGestureRecognizerState.ended {
                UIView.animate(withDuration: 1, animations: {
                    sender.view!.center.x = 350
                }, completion: { (true) in
                    UIView.animate(withDuration: 1, animations: {
                        self.balloon.center.y = -200
                        self.anvil.center.y = 280
                    }, completion: { (true) in
                        UIView.animate(withDuration: 3, animations: {
                            self.flame.center.y = 640
                        }, completion: { (true) in
                            UIView.animate(withDuration: 2, animations: {
                                self.flame.center.x = 145
                            }, completion: { (true) in
                                UIView.animate(withDuration: 1, delay: 1, options: [.transitionCrossDissolve], animations: {
                                    self.box.backgroundColor = .red
                                }, completion: { (true) in
                                    UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 10, animations: {
                                            self.tyler.center.y -= 100
                                    }, completion: nil)
                                })
                            })
                        })
                    })
                })
            }
        }
        //setnewCGon stop
        //sender.setTranslation(CGPoint.zero, in: self.view)
    }
}



//    @IBAction func handlePan(recognizer:UIPanGestureRecognizer) {
//        let translation = recognizer.translation(in: self.view)
//        if let view = recognizer.view {
//            view.center = CGPoint(x:view.center.x + translation.x,
//                                  y:view.center.y + translation.y)
//        }
//        //setnewstop
//        recognizer.setTranslation(CGPoint.zero, in: self.view)
//
//    if recognizer.state == UIGestureRecognizerState.ended {
//        UIView.animate(withDuration: 1, animations: {
//            recognizer.arrow.center.x += 100
//        }, completion: nil)//{ (true) in

//        }
//    }
//}

